import Func
def simpson(n,a,b,param):
    h=(b-a)/(2*n)
    s1=0
    s2=0
    x=a+h
    count=1
    while x<b:
        if count%2==0:
            s1+=Func.myFunc(x,param)
        else:
            s2+=Func.myFunc(x,param)
        count+=1
        x+=h
    I=h/3 *(Func.myFunc(a,param)+Func.myFunc(b,param) +2*s1+4*s2)
    return(I)
