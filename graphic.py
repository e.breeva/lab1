import matplotlib.pyplot as plt
import csv
def repl(s):
    return(str(s).replace('.',','))
def save(spisok):
    for i in range(len(spisok)-1):
        if i!=0:
            try:
                spisok[i]=repl(spisok[len(spisok)-1]-spisok[i])
            except:
                spisok[i]=spisok[len(spisok)-1]-spisok[i]
    spisok[0]=str(spisok[0])
    spisok.pop()    
    try:
        file=open('dlyagraf.csv','x')
    except:
        file=open("dlyagraf.csv",'a',newline="")
    file_writer=csv.writer(file,delimiter=";",lineterminator="\r",)    
    file_writer.writerow(spisok)
    file.close()
def loadgraf():
    gry=[]
    gry1=[]
    gry2=[]
    gry3=[]
    gry4=[]
    grx=[] 
    file=open("dlyagraf.csv")
    file_reader=csv.reader(file,delimiter=";")
    for row in file_reader:
        grx.append(int(row[0]))
        gry.append(float(row[1].replace(',','.')))
        gry1.append(float(row[2].replace(',','.')))
        gry2.append(float(row[3].replace(',','.')))
        gry3.append(float(row[4].replace(',','.')))
        gry4.append(float(row[5].replace(',','.')))
    file.close()
    plt.xlabel('количество разбиений')
    plt.ylabel('разность между истинным значением и приближенным')
    plt.grid()
    plt.plot(grx,gry,grx,gry1,grx,gry2,grx,gry3,grx,gry4)
    plt.legend(["левые прямоугольники","правые прямоугольники","средние прямоугольники","трапеции","симпсона"])
    plt.show()
