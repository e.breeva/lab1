import Func
def trap(n,a,b,param):
    h=(b-a)/n
    suma=0
    x=a+h
    while x<b:
        suma+=Func.myFunc(x,param)
        x+=h
    I=h*((Func.myFunc(a,param)+Func.myFunc(b,param))/2 +suma)
    return(I)
