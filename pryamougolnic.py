import Func
def leftpryam(n,a,b,param):
    x=a
    h=(b-a)/n
    suma=0
    while x<b:
        suma+=Func.myFunc(x,param)
        x+=h
    I=h*suma
    return(I)
def rightpryam(n,a,b,param):    
    h=(b-a)/n
    suma=0
    x=a+h
    while x<=b:
        suma+=Func.myFunc(x,param)
        x+=h
    I=h*suma
    return(I)
def centerpryam(n,a,b,param):    
    h=(b-a)/n
    suma=0
    x=a
    while x<b:
        xc=x+h/2
        suma+=Func.myFunc(xc,param)
        x+=h
    I=h*suma
    return(I)
