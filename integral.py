import pryamougolnic as pr
import simpson as sm
import trapec as tr
import graphic as gr
a = 1 #float(input("Введите A: "))
b = 5#float(input("Введите B: "))
n = int(input("Введите количество отрезков: "))
param=3#float(input("Введите параметр а: "))
spisok=[]
spisok.append(n)
spisok.append(pr.leftpryam(n,a,b,param))
spisok.append(pr.rightpryam(n,a,b,param))
spisok.append(pr.centerpryam(n,a,b,param))
spisok.append(tr.trap(n,a,b,param))
spisok.append(sm.simpson(n,a,b,param))
sp=[-0.794356821036392,-0.794361547952134,-0.794345768210362]
sp.remove(min(sp))
sp.remove(max(sp))
con=sp[len(sp)-1]
spisok.append(con)
print(spisok)
gr.save(spisok)
gr.loadgraf()
